#!/usr/bin/env python3

# Go through all the datasets and get stats about them.
# The output will be valid python.

import json
import os
import sys
import traceback

DEFAULT_DATA_DIR = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'tests', 'data', 'seed_datasets_current')

TASK_TYPES = {
    "classification",
    "regression",
    "linkPrediction",
    "vertexNomination",
    "vertexClassification",
    "graphClustering",
    "graphMatching",
    "timeSeries",
    "forecasting",
    "collaborativeFiltering",
    "objectDetection",
    "communityDetection"
}

def linecount(path):
    with open(path, 'r') as file:
        return len(file.readlines())

def get_json(path):
    with open(path, 'r') as file:
        return json.load(file)

def parse_dataset(dataset_name, dataset_path):
    dataset = {}
    dataset['name'] = dataset_name

    dataset_doc = get_json(os.path.join(dataset_path, "%s_dataset" % (dataset_name), 'datasetDoc.json'))
    dataset['dataset_id'] = dataset_doc['about']['datasetID']

    problem_doc = get_json(os.path.join(dataset_path, "%s_problem" % (dataset_name), 'problemDoc.json'))

    dataset['problem_id'] = problem_doc['about']['problemID']

    taskKeywords = problem_doc['about']['taskKeywords']
    taskType = TASK_TYPES.intersection(taskKeywords)

    if len(taskType) == 1:
        dataset['task_type'] = taskType.pop()
    elif len(taskType) == 0:
        raise ValueError("Supported task type not found in taskKeywords from problemDoc (%s)." % (taskKeywords))
    elif taskType.__contains__("timeSeries") & taskType.__contains__("forecasting"):
        dataset['task_type'] = "timeseriesForecasting"
    elif taskType.__contains__("timeSeries") & taskType.__contains__("classification"):
        dataset['task_type'] = "timeseriesClassification"
    elif len(taskType) > 1:
        raise ValueError("More than one supported task types (%s) found in taskKeywords from problemDoc (%s)." % (taskType, taskKeywords))

    # Datasets (as of 2019-06-06) appear to follow two different conventions concerning where targets are specified.
    # We try to allow for both conventions
    path = os.path.join(dataset_path, 'SCORE', 'targets.csv')
    if not os.path.exists(path):
        path = os.path.join(dataset_path, 'SCORE', 'problem_SCORE', 'dataSplits.csv')
    dataset['size'] = linecount(path) - 1

    dataset_doc = get_json(os.path.join(dataset_path, 'TRAIN', 'dataset_TRAIN', 'datasetDoc.json'))
    dataset['train_dataset_id'] = dataset_doc['about']['datasetID']

    problem_doc = get_json(os.path.join(dataset_path, 'TRAIN', 'problem_TRAIN', 'problemDoc.json'))
    dataset['train_problem_id'] = problem_doc['about']['problemID']

    dataset_doc = get_json(os.path.join(dataset_path, 'TEST', 'dataset_TEST', 'datasetDoc.json'))
    dataset['test_dataset_id'] = dataset_doc['about']['datasetID']

    problem_doc = get_json(os.path.join(dataset_path, 'TEST', 'problem_TEST', 'problemDoc.json'))
    dataset['test_problem_id'] = problem_doc['about']['problemID']

    # There are two different ways in which the datasets record the scoring dataset and problem.
    # We'll try each.
    path = os.path.join(dataset_path, 'SCORE', 'dataset_SCORE', 'datasetDoc.json')
    if not os.path.exists(path):
        path = os.path.join(dataset_path, 'SCORE', 'dataset_TEST', 'datasetDoc.json')
    dataset_doc = get_json(path)
    # There is currently an issue where old datasets have an old id - all scoring problem ids should be set to _SCORE
    # even though the field in the dataset doc is wrong. See https://gitlab.com/datadrivendiscovery/d3m/blob/devel/d3m/runtime.py#L1558
    score_path_id = dataset_doc['about']['datasetID']
    score_path_id = score_path_id.replace("TEST", "SCORE")
    dataset['score_dataset_id'] = score_path_id

    path = os.path.join(dataset_path, 'SCORE', 'problem_SCORE', 'problemDoc.json')
    if not os.path.exists(path):
        path = os.path.join(dataset_path, 'SCORE', 'problem_TEST', 'problemDoc.json')
    problem_doc = get_json(path)
    # There is currently an issue where old datasets have an old id - all scoring problem ids should be set to _SCORE
    # even though the field in the dataset doc is wrong. See https://gitlab.com/datadrivendiscovery/d3m/blob/devel/d3m/runtime.py#L1558
    score_path_id = problem_doc['about']['problemID']
    score_path_id = score_path_id.replace("TEST", "SCORE")
    dataset['score_problem_id'] = score_path_id

    return dataset

def parse_datasets(data_dir):
    datasets = []
    bad_datasets = []

    for dirent in os.listdir(data_dir):
        dirent_path = os.path.join(data_dir, dirent)

        if (os.path.isfile(dirent_path)):
            continue

        try:
            datasets.append(parse_dataset(dirent, dirent_path))
        except Exception as ex:
            bad_datasets.append({
                'name': dirent,
                'error': str(ex),
            })

            print("Error parsing: %s" % (dirent_path), file = sys.stderr)
            traceback.print_exc(file = sys.stderr)
            print('---', file = sys.stderr)

    return datasets, bad_datasets

def main():
    data_dir = None
    if len(sys.argv) > 1:
        data_dir = sys.argv[1]
    else:
        data_dir = DEFAULT_DATA_DIR

    datasets, bad_datasets = parse_datasets(data_dir)

    print('import json')
    print()
    print("# Generated by %s." % (__file__))
    print("ALL_DATASETS_JSON = '''")
    print(json.dumps(datasets, indent = 4))
    print("'''")
    print()
    print("BAD_DATASETS_JSON = '''")
    print(json.dumps(bad_datasets, indent = 4))
    print("'''")
    print()
    print('ALL_DATASETS = json.loads(ALL_DATASETS_JSON)')

if (__name__ == '__main__'):
    main()
