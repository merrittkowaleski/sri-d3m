#!/bin/bash

readonly CONFIG_PATH='sri/common/config.py'

readonly TA1_SRI_VERSION=$(grep '^VERSION' "${CONFIG_PATH}" | sed "s/^.*'\(.*\)'.*/\1/")
readonly TA1_API_VERSION=$(grep '^D3M_API_VERSION' "${CONFIG_PATH}" | sed "s/^.*'\(.*\)'.*/\1/")
readonly TEAMNAME=$(grep '^D3M_PERFORMER_TEAM' "${CONFIG_PATH}" | sed "s/^.*'\(.*\)'.*/\1/")

function genPrimitive() {
    local teamDir=$1
    local primitive=$2

    outDir="${teamDir}/${primitive}/${TA1_SRI_VERSION}"

    # Generate the pipeline.json
    echo "python3 -m d3m index describe -i 4 ${primitive} > ${outDir}/primitive.json"
    mkdir -p "${outDir}"
    python3 -m d3m index describe -i 4 "${primitive}" > "${outDir}/primitive.json"

    # Pipelines
    pipelineOutDir="${outDir}/pipelines"
    mkdir -p "${pipelineOutDir}"
    # Pipeline Runs
    pipelineRunOutDir="${outDir}/pipeline_runs"
    echo "mkdir -p ${pipelineRunOutDir}"
    mkdir -p "${pipelineRunOutDir}"

    # Generate the sample pipelines for each valid dataset
    echo "python3 -m sri.pipelines.output_primitive --prediction ${primitive} ${pipelineOutDir} ${pipelineRunOutDir}"
    python3 -m sri.pipelines.output_primitive --prediction "${primitive}" "${pipelineOutDir}" "${pipelineRunOutDir}"

    # Cleanup by removing empty dirs.
    find "${outDir}" -type d -empty -delete
}


function main() {
    trap exit SIGINT
    set -e

    if [[ $# -ne 1 ]]; then
        echo "USAGE: ${0} <primitive repo dir>"
        exit 1
    fi

    local repoDir="${1}"
    local teamDir="${repoDir}/${TA1_API_VERSION}/${TEAMNAME}"

    rm -Rf "${teamDir}"
    mkdir -p "${teamDir}"

    for primitive in $(python3 -m sri.common.entrypoints); do
        genPrimitive "${teamDir}" "${primitive}"
    done
}

main "$@"
