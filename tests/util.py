import os
import json
import d3m.metadata.base as md_base
from sri.common.constants import SUGGESTED_TARGET_TYPE

DATA_DIR = os.path.join('tests', 'data')
SEED_DATA_DIR = os.path.join(DATA_DIR, 'seed_datasets_current')

TRAIN_PATH_SUFFIX = os.path.join('TRAIN', 'dataset_TRAIN', 'datasetDoc.json')
TEST_PATH_SUFFIX = os.path.join('TEST', 'dataset_TEST', 'datasetDoc.json')

TRAIN_PROBLEM_PATH_SUFFIX = os.path.join('TRAIN', 'problem_TRAIN', 'problemDoc.json')
TEST_PROBLEM_PATH_SUFFIX = os.path.join('TEST', 'problem_TEST', 'problemDoc.json')

def getTrainPath(dataset):
    return os.path.abspath(os.path.join(SEED_DATA_DIR, dataset, TRAIN_PATH_SUFFIX))

def getTestPath(dataset):
    return os.path.abspath(os.path.join(SEED_DATA_DIR, dataset, TEST_PATH_SUFFIX))

def getTrainProblemPath(dataset):
    return os.path.abspath(os.path.join(SEED_DATA_DIR, dataset, TRAIN_PROBLEM_PATH_SUFFIX))

def getTestProblemPath(dataset):
    return os.path.abspath(os.path.join(SEED_DATA_DIR, dataset, TEST_PROBLEM_PATH_SUFFIX))

def getScoreBaseDir(dataset):
    return os.path.abspath(os.path.join(SEED_DATA_DIR, dataset, 'SCORE'))

def add_dataset_tests(test_object, test_method_name, method_label, *datasets):
    for dataset in datasets:
        def test_method(self):
            method = getattr(self, test_method_name)
            method(dataset)
        test_name = 'test_%s_dataset_%s' % (method_label, dataset)
        setattr(test_object, test_name, test_method)

def get_suggested_target_resource_and_column(problem_doc_path):
    with open(problem_doc_path) as fh:
        json_obj = json.load(fh)
    targ_rec = json_obj['inputs']['data'][0]['targets'][0]
    return targ_rec['resID'], targ_rec['colIndex']

def set_suggested_target(dataset, problem_doc_path):
    resource, target_column_index = get_suggested_target_resource_and_column(problem_doc_path)
    dataset.metadata = dataset.metadata.add_semantic_type((resource, md_base.ALL_ELEMENTS, target_column_index),
                                                          SUGGESTED_TARGET_TYPE)

