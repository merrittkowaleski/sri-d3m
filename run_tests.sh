#!/bin/bash

TEST_DIR='tests'
DATA_DIR="${TEST_DIR}/data"

SEED_DIR='seed_datasets_current'
DOWNLOAD_FILE='.downloaded'
DATASETS_REPO='git@gitlab.datadrivendiscovery.org:d3m/datasets.git'
# We now requre all datasets.
REQUIRED_DATASETS=`ls "${DATA_DIR}/${SEED_DIR}/"`

# Set some standard D3M variables.

export D3MCPU=$(( $(nproc) < 2 ? 1 : $(nproc) - 1 ))

AVAILABLE_MEM_KB=$(cat /proc/meminfo | grep 'MemTotal' | sed 's/^[^0-9]\+\([0-9]\+\)[^0-9]\+$/\1/')
# Floor by multiples of 3 and then reserve an additional 3 GB.
export D3MRAM=$((${AVAILABLE_MEM_KB} / 1024 / 1024 / 3 * 3 - 3))G

function fetchData() {
   # Ensure that the data directory exists (and is a git repo).
   if [ -d "${DATA_DIR}" ]; then
      if [ ! -d "${DATA_DIR}/.git" ]; then
         echo "Data dir exists, but is not a git repo!"
         exit 2
      fi
   else
      # Do a base checkout without getting large files.
      git lfs clone "${DATASETS_REPO}" -X "*" "${DATA_DIR}"
      pushd . > /dev/null
      cd "${DATA_DIR}"
      git lfs install
      popd > /dev/null
   fi

   pushd . > /dev/null
   cd "${DATA_DIR}"
   for dataset in $REQUIRED_DATASETS; do
      flagFile="${SEED_DIR}/${dataset}/${DOWNLOAD_FILE}"
      if [ ! -f "${flagFile}" ]; then
         echo "Flag for ${dataset} does not exist, downloading..."
         git lfs pull -I "${SEED_DIR}/${dataset}/"
         touch "${flagFile}"
      fi
   done
   popd > /dev/null
}

function runTests() {
   python3 run_tests_internal.py "$@"
}

function main() {
   trap exit SIGINT
   set -e

   buildInstall
   fetchData
   runTests "$@"
}

function buildInstall() {
   local version=`python3 setup.py sdist | grep -P 'removing.*(and everything under it)' | sed "s/removing '\(.*\)' (and everything under it)/\1/"`
   echo "Using SRI TA1 version ${version}."
   pip3 install --no-deps --upgrade "dist/${version}.tar.gz"
}

main "$@"
